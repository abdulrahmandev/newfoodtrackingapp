package com.healthifyme.kadkab.foodtrackingusingsharedelements;

import android.app.Activity;
import android.os.Bundle;
import android.support.v4.view.MotionEventCompat;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.CardView;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.view.animation.TranslateAnimation;
import android.widget.ImageView;

/**
 * Created by kadkab on 1/3/2016.
 */
public class FoodTrackGraphActivity extends AppCompatActivity {

    private ImageView imageView;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.second_screen);

        //Animation for image view not working :(.
        imageView = (ImageView) findViewById(R.id.graph);
//        Animation bottomUp = AnimationUtils.loadAnimation(getApplication(),
//                R.anim.bottomup);
//        bottomUp.start();
        imageView.setVisibility(View.VISIBLE);

//        TranslateAnimation animate = new TranslateAnimation(0,0,0,-imageView.getHeight());
//        animate.setDuration(500);
//        animate.setFillAfter(true);
//        imageView.startAnimation(animate);
//        imageView.setVisibility(View.VISIBLE);


        CardView cardView = (CardView) findViewById(R.id.card_view_new);
        cardView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackPressed();
            }
        });

        cardView.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View view, MotionEvent motionEvent) {
                float initialY=0f;
                float finalY=0f;
                if(MotionEventCompat.getActionMasked(motionEvent) == MotionEvent.ACTION_DOWN){
                    initialY = motionEvent.getY();
                }
                if(MotionEventCompat.getActionMasked(motionEvent) == MotionEvent.ACTION_UP){
                    finalY = motionEvent.getY();
                    if(initialY<finalY) {
                        if(imageView.getVisibility()==View.VISIBLE){
                            onBackPressed();
                        }else{
                            imageView.setVisibility(View.VISIBLE);
                        }
                        return true;
                    }else if(initialY>finalY) {
                        imageView = (ImageView) findViewById(R.id.graph);
//                        Animation bottomUp = AnimationUtils.loadAnimation(getApplication(),
//                                R.anim.bottomup);
//                        imageView.startAnimation(bottomUp);
                        imageView.setVisibility(View.GONE);

                        return true;
                    }
                }
                return false;
            }

        });
    }
}
